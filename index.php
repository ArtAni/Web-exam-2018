<!DOCTYPE html>
<html>
<head>
    <title>Главная страница</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>
<body>
	<?php include("template/header.php");  ?>
	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
		    <div class="carousel-item active">
		        <img class="d-block w-100" src="img/x_antivirus_.jpg.pagespeed.ic.VEL4xENUmt.jpg" alt="First slide">
		    </div>
		    <div class="carousel-item">
		        <img class="d-block w-100" src="img/xbitrix.png.pagespeed.ic.U1ylpgiBpH.jpg" alt="Second slide">
		    </div>
		    <div class="carousel-item">
		        <img class="d-block w-100" src="img/xgodbezzabot2.png.pagespeed.ic.Swcas93Res.jpg" alt="Third slide">
		    </div>
		</div>
		    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
		      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		      <span class="sr-only">Previous</span>
		    </a>
		    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
		      <span class="carousel-control-next-icon" aria-hidden="true"></span>
		      <span class="sr-only">Next</span>
		    </a>
	</div>
	<div class="container-fluid">
		<div class="row justify-content-md-center text-center">
			<div class="col-10">
				<h3 class="text-gilroy text-black text-left">Наши преимущества</h3>
			</div>
		
			<div class="w-100"></div>
			<div class="col-lg-2 col-12">
				<div class="row">
					<div class="col-12 text-left"><h5 class="text-gilroy">Лучшие специалисты в области</h5></div>
					<div class="col-12 text-gilroy">Больше 10 лет мы создаем для вас хостинг и знаем про него практически всё.</div>
				</div>
			</div>
			<div class="col-lg-2 col-12">
				<div class="row">
					<div class="col-12 text-left"><h5 class="text-gilroy">Технологические иновации</h5></div>
					<div class="col-12 text-gilroy">Мы обеспечиваем высокую скорость и стабильность работы ваших сайтов за счет использования самого современного оборудования и программных систем.</div>
				</div>
			</div>
			<div class="col-lg-2 col-12">
				<div class="row">
					<div class="col-12 text-left"><h5 class="text-gilroy">Иновационная панель управления</h5></div>
					<div class="col-12 text-gilroy">Мы разработали собственную панель управления на основе ваших пожеланий</div>
				</div>
			</div>
			<div class="col-lg-2 col-12">
				<div class="row">
					<div class="col-12 text-left"><h5 class="text-gilroy">Безопасность данных</h5></div>
					<div class="col-12 text-gilroy">Данные ваших сайтов надежно защищены системой резервирования, а Kaspersky надежно защищает вашу почту.</div>
				</div>
			</div>
			<div class="col-lg-2 col-12">
				<div class="row">
					<div class="col-12 text-left"><h5 class="text-gilroy">Выгодыне тарифы</h5></div>
					<div class="col-12 text-gilroy">Мы ценим ваш выбор и предлагаем лучшие условия сотрудничества - выгодные тарифы и бонусы для наших клиентов!</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row justify-content-md-center text-center">
			<div class="col-lg-3 col-md-6 col-12">
				<div class="row">
					<div class="col-12">
						<img src="img/xDepositphotos_186840840_l-2015.jpg.pagespeed.ic.hStCkKf_6N.jpg" class="img-fluid">
					</div>
					<div class="col-12 text-gilroy">Приведи друга и получите подарок!</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-12">
				<div class="row">
					<div class="col-12">
						<img src="img/xDepositphotos_52980661_l-2015.jpg.pagespeed.ic.Ho31Uay5FE.jpg" class="img-fluid">
					</div>
					<div class="col-12 text-gilroy">2 года хостинга в подарок при покупке 1С-Бритркс!</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-12">
				<div class="row">
					<div class="col-12">
						<img src="img/xDepositphotos_48832935_l-2015.jpg.pagespeed.ic.QCtobj3hDs.jpg" class="img-fluid">
					</div>
					<div class="col-12 text-gilroy">Все готово к переезду: 3 месяца хостинга в подарок!</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-12">
				<div class="row">
					<div class="col-12">
						<img src="img/xDepositphotos_24601083_l-2015.jpg.pagespeed.ic.Kk8lZMDNbm.jpg" class="img-fluid">
					</div>
					<div class="col-12 text-gilroy">Хотите сменить хостера? Подарим остаток баланса на счет!</div>
				</div>
			</div>
		</div>
	</div>
	<?php include("template/footer.php");  ?>
	

</body>