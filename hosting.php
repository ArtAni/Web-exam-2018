<!DOCTYPE html>
<html>
<head>
    <title>Хостинг</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>
<body>
	<?php include("template/header.php");  ?>
	<div class="container">
		<div class="row h-100">
			<div class="col-lg-3 col-12 variable-bg-red text-gilroy">
				<h3 class="text-gilroy">Year</h3>
				<p class="text-gilroy">1 месяц за 100 ₽</p>
				<p class="text-gilroy">5 Gb места</p>
				<p class="text-gilroy">1 сайт / ftp акк.</p>
				<p>1 база данных</p>

				<p>3 резервных копии</p>
				<p>круглосуточная поддержка</p>
				<p>10 Гб для почты</p>
				<p>SEO и реклама </p>
				<p>в панели хостинга</p>
				<p>бесплатный </p>
				<p>SSL-сертификат</p>
				<p>бесплатный </p>
				<p>перенос сайтов </p>
				<p>от другого хостера</p>
				<form method="post">
				  	<div class="form-row">
				    	<div class="col-12">
				    		Выберите количество месяцев
				      		<input type="number" class="form-control" placeholder="Выберите количество месяцев" value="1" min="1" name="Year-month">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительный обьём
				      		<input type="number" class="form-control" placeholder="Выберите дополнительный обьём трафика" min="0" max="30" name="Year-volume">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительное количество баз данных
				      		<input type="number" class="form-control" placeholder="Выберите дополнительное количество баз данных" min="0" max="5" name="Year-bd">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительное количество сайтов
				      		<input type="number" class="form-control" placeholder="Выберите дополнительное количество сайтов" min="0" max="5" name="Year-sites">
				    	</div>
				    	<div class="col-12">
				    		<button type="submit" class="btn btn-primary text-center w-100" name="Year">Выбрать</button>
				    	</div>
				  	</div>
				</form>



			</div>
			<div class="col-lg-3 col-12 variable-bg-blue text-gilroy">
				<h3>Optimo</h3>
				<p>1 месяц за 250 ₽</p>
				<p>10 Gb места</p>
				<p>10 сайтов / ftp акк.</p>
				<p>10 баз данных</p>

				<p>3 резервных копии</p>
				<p>круглосуточная поддержка</p>
				<p>10 Гб для почты</p>
				<p>SEO и реклама </p>
				<p>в панели хостинга</p>
				<p>бесплатный </p>
				<p>SSL-сертификат</p>
				<p>бесплатный </p>
				<p>перенос сайтов </p>
				<p>от другого хостера</p>
				<form method="post">
				  	<div class="form-row">
				    	<div class="col-12">
				    		Выберите количество месяцев
				      		<input type="number" class="form-control" placeholder="Выберите количество месяцев" value="1" min="1" name="Optimo-month">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительный обьём
				      		<input type="number" class="form-control" placeholder="Выберите дополнительный обьём трафика" min="0" max="30" name="Optimo-volume">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительное количество баз данных
				      		<input type="number" class="form-control" placeholder="Выберите дополнительное количество баз данных" min="0" max="5" name="Optimo-bd">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительное количество сайтов
				      		<input type="number" class="form-control" placeholder="Выберите дополнительное количество сайтов" min="0" max="5" name="Optimo-sites">
				    	</div>
				    	<div class="col-12">
				    		<button type="submit" class="btn btn-primary text-center w-100" name="Optimo">Выбрать</button>
				    	</div>
				  	</div>
				</form>


			</div>
			<div class="col-lg-3 col-12 variable-bg-green text-gilroy">
				<h3>Century</h3>
				<p>1 месяц за 400 ₽</p>
				<p>15 Gb места</p>
				<p>25 сайтов / ftp акк.</p>
				<p>25 баз данных</p>

				<p>3 резервных копии</p>
				<p>круглосуточная поддержка</p>
				<p>10 Гб для почты</p>
				<p>SEO и реклама </p>
				<p>в панели хостинга</p>
				<p>бесплатный </p>
				<p>SSL-сертификат</p>
				<p>бесплатный </p>
				<p>перенос сайтов </p>
				<p>от другого хостера</p>
				<form method="post">
				  	<div class="form-row">
				    	<div class="col-12">
				    		Выберите количество месяцев
				      		<input type="number" class="form-control" placeholder="Выберите количество месяцев" value="1" min="1" name="Century-month">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительный обьём 
				      		<input type="number" class="form-control" placeholder="Выберите дополнительный обьём трафика" min="0" max="30" name="Century-bd">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительное количество баз данных
				      		<input type="number" class="form-control" placeholder="Выберите дополнительное количество баз данных" min="0" max="5" name="Century-bd">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительное количество сайтов
				      		<input type="number" class="form-control" placeholder="Выберите дополнительное количество сайтов" min="0" max="5" name="Century-sites">
				    	</div>
				    	<div class="col-12">
				    		<button type="submit" class="btn btn-primary text-center w-100" name="Century">Выбрать</button>
				    	</div>
				  	</div>
				</form>


			</div>
			<div class="col-lg-3 col-12 variable-bg-purple text-gilroy">
				<h3>Millennium</h3>
				<p>1 месяц за 570 ₽</p>
				<p>25 Gb места</p>
				<p>50 сайтов / ftp акк.</p>
				<p>∞ баз данных</p>

				<p>3 резервных копии</p>
				<p>круглосуточная поддержка</p>
				<p>10 Гб для почты</p>
				<p>SEO и реклама </p>
				<p>в панели хостинга</p>
				<p>бесплатный </p>
				<p>SSL-сертификат</p>
				<p>бесплатный </p>
				<p>перенос сайтов </p>
				<p>от другого хостера</p>
				<form method="post">
				  	<div class="form-row">
				    	<div class="col-12">
				    		Выберите количество месяцев
				      		<input type="number" class="form-control" placeholder="Выберите количество месяцев" value="1" min="1" name="Millennium-month">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительный обьём
				      		<input type="number" class="form-control" placeholder="Выберите дополнительный обьём трафика" min="0" max="30" name="Millennium-volume">
				    	</div>
				    	<div class="col-12">
				    		Выберите дополнительное количество сайтов
				      		<input type="number" class="form-control" placeholder="Выберите дополнительное количество сайтов" min="0" max="5" name="Millennium-sites">
				    	</div>
				    	<div class="col-12">
				    		<button type="submit" class="btn btn-primary text-center w-100" name="Millennium">Выбрать</button>
				    	</div>
				  	</div>
				</form>

				
			</div>
			<div class="col-12">
				<?php  
					if (isset($_POST['Year'])) {
						$rez = 100 * $_POST['Year-month'];
						$rez = $rez;
						$dop_domen = 0;
						$dop_volume = $rez * $_POST['Year-volume'] * 0.05;
						$dop_bd = $rez * $_POST['Year-bd'] * 0.05;
						$dop_sites = $_POST['Year-sites'] + 100;
						$rez = $rez + $dop_volume + $dop_bd; 




						if ($_POST['Year-month'] == 1) {
							$skid = $rez * 0;
						}
						if ($_POST['Year-month'] >= 2) {
							$skid = $rez * 0.05;
						}
						if ($_POST['Year-month'] >= 3) {
							$skid = $rez * 0.07;
						}if ($_POST['Year-month'] >= 6) {
							$skid = $rez * 0.09;
						}
						if ($_POST['Year-month'] >= 12) {
							$skid = $rez * 0.1;
							$dop_domen += 1;
						}
						$rez_skid = $rez - $skid;
						if ($rez_skid >= 5000) {
							$dop_domen += 1;
						}
						$dop_sites = $_POST['Year-sites'] * 100;
						$rez += $dop_sites;
						$rez_skid += $dop_sites; 
						echo "<p>Цена без скидки ".$rez."</p>";
						echo "<p>Цена со скидкой ".$rez_skid."</p>";
						if ($dop_domen != 0) {
							echo "Поздравляю, вы дополнительные домены в размере:".$dop_domen."</p>";
						}

						
					}elseif (isset($_POST['Optimo'])) {

						$rez = 250 * $_POST['Optimo-month'];
						$rez = $rez;
						$dop_domen = 0;
						$dop_volume = $rez * $_POST['Optimo-volume'] * 0.05;
						$dop_bd = $rez * $_POST['Optimo-bd'] * 0.05;
						$dop_sites = $_POST['Optimo-sites'] + 100;
						$rez = $rez + $dop_volume + $dop_bd; 




						if ($_POST['Optimo-month'] == 1) {
							$skid = $rez * 0;
						}
						if ($_POST['Optimo-month'] >= 2) {
							$skid = $rez * 0.05;
						}
						if ($_POST['Optimo-month'] >= 3) {
							$skid = $rez * 0.07;
						}if ($_POST['Optimo-month'] >= 6) {
							$skid = $rez * 0.09;
						}
						if ($_POST['Optimo-month'] >= 12) {
							$skid = $rez * 0.1;
							$dop_domen += 1;
						}
						$rez_skid = $rez - $skid;
						if ($rez_skid >= 5000) {
							$dop_domen += 1;
						}
						$dop_sites = $_POST['Optimo-sites'] * 100;
						$rez += $dop_sites;
						$rez_skid += $dop_sites; 
						echo "<p>Цена без скидки ".$rez."</p>";
						echo "<p>Цена со скидкой ".$rez_skid."</p>";
						if ($dop_domen != 0) {
							echo "Поздравляю, вы дополнительные домены в размере:".$dop_domen."</p>";
						}

						
					}elseif (isset($_POST['Century'])) {
						$rez = 400 * $_POST['Century-month'];
						$rez = $rez;
						$dop_domen = 0;
						$dop_volume = $rez * $_POST['Century-volume'] * 0.05;
						$dop_bd = $rez * $_POST['Century-bd'] * 0.05;
						$dop_sites = $_POST['Century-sites'] + 100;
						$rez = $rez + $dop_volume + $dop_bd; 




						if ($_POST['Century-month'] == 1) {
							$rskid = $rez * 0;
						}
						if ($_POST['Century-month'] >= 2) {
							$skid = $rez * 0.03;
						}
						if ($_POST['Century-month'] >= 3) {
							$skid = $rez * 0.04;
						}if ($_POST['Century-month'] >= 6) {
							$skid = $rez * 0.05;
						}
						if ($_POST['Century-month'] >= 12) {
							$skid = $rez * 0.1;
							$dop_domen += 1;
						}
						$rez_skid = $rez - $skid;
						if ($rez_skid >= 5000) {
							$dop_domen += 1;
						}

						$dop_sites = $_POST['Century-sites'] * 100;
						$rez += $dop_sites;
						$rez_skid += $dop_sites; 
						echo "<p>Цена без скидки ".$rez."</p>";
						echo "<p>Цена со скидкой ".$rez_skid."</p>";
						if ($dop_domen != 0) {
							echo "Поздравляю, вы дополнительные домены в размере:".$dop_domen."</p>";
						}

						
					}elseif (isset($_POST['Millennium'])) {
						$rez = 570 * $_POST['Millennium-month'];
						$rez = $rez;
						$dop_domen = 0;
						$dop_volume = $rez * $_POST['Millennium-volume'] * 0.05;
						$dop_bd = $rez * 0 * 0.05;
						$dop_sites = $_POST['Millennium-sites'] + 100;
						$rez = $rez + $dop_volume + $dop_bd; 




						if ($_POST['Millennium-month'] == 1) {
							$skid = $rez * 0;
						}
						if ($_POST['Millennium-month'] >= 2) {
							$skid = $rez * 0.03;
						}
						if ($_POST['Millennium-month'] >= 3) {
							$skid = $rez * 0.04;
						}if ($_POST['Millennium-month'] >= 6) {
							$skid = $rez * 0.05;
						}
						if ($_POST['Millennium-month'] >= 12) {
							$skid = $rez * 0.1;
							$dop_domen += 1;
						}
						$rez_skid = $rez - $skid;
						if ($rez_skid >= 5000) {
							$dop_domen += 1;
						}
						$dop_sites = $_POST['Millennium-sites'] * 100;
						$rez += $dop_sites;
						$rez_skid += $dop_sites; 

						echo "<p>Цена без скидки ".$rez."</p>";
						echo "<p>Цена со скидкой ".$rez_skid."</p>";
						if ($dop_domen != 0) {
							echo "Поздравляю, вы дополнительные домены в размере:".$dop_domen."</p>";
						}


					
					}



				?>
			</div>
		</div>
	</div>






	<?php include("template/footer.php");  ?>

</body>
</html>