<div class="container-fluid main-bg-black">
	<div class="row text-right text-gilroy">
		<div class="col-12" style="height: 5em;"></div>
		<div class="col-12 text-verdana text-white">© 2006-2018 ООО «ТаймВэб».</div>
		<div class="col-12 text-verdana text-white">Зарегистрированный товарный знак N461919. Лицензия РОСКОМНАДЗОР N142739. </div>
		<div class="col-12 text-verdana text-white">Сайт работает на «1С-Битрикс».</div>
		<div class="col-12 text-verdana text-white">Фактический адрес: Россия, 196084, город Санкт-Петербург, улица Заставская, дом 22, корпус 2, литера А</div>
		<div class="col-12 text-verdana text-white">Политика ООО «Таймвэб» в отношении обработки персональных данных</div>
	</div>
</div>