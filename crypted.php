<!DOCTYPE html>
<html>
<head>
    <title>Шифрование</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/webexam-crypto.js"></script>
</head>
<body>

	<?php include("template/header.php");  ?>
	<div class="card">
  		<div class="card-header">Шифрование</div>
  		<div class="card-body">
    		<form name="Crypt">
				  	<div class="form-row text-gilroy">
				  		Ключ AES-256-CBC:
				  		<div class="col-12 pt-2 text-gilroy"><input type="text" name="key" class="form-control" id="key"></div>
				  		<div class="col-12 pt-2 text-gilroy"><input type="button" class="btn btn-primary text-center" name="generate" value="Сгенерировать сключ" onclick="generateBtn()"></div>
				  		<div class="col-md-6 col-12">
				  			<div class="form-row text-gilroy">
				  				Исходный текст:
						  		<div class="col-12 pt-2 text-gilroy"><input class="form-control" id="opentext" name="opentext"></div>
						  		<div class="col-12 pt-2 text-gilroy"><input type="reset" class="btn btn-primary text-center" name="clear" value="Очистить" id="clear"></div>
						  		<div class="col-12 pt-2 text-gilroy"><input type="button" class="btn btn-primary text-center" name="encrypt" value="Зашифровать" onclick="crypt()"></div>
				  			</div>
				  		</div>
				  		<div class="col-md-6 col-12">
				  			<div class="form-row text-gilroy">
				  				Зашифрованное сообщение (выводиться в формате HEX(IV + MSG)):
						  		<div class="col-12 pt-2 text-gilroy"><input class="form-control" id="crypttext" name="crypttext"></div>
						  		<div class="col-12 pt-2 text-gilroy"><input type="button" class="btn btn-primary text-center" name="decrypt" value="Расшифровать" onclick="decrypted()"></div>
				  			</div>
				  		</div>
				  		<div class="col-12">
				  			<div class="form-row text-gilroy">
				  				Расшифрованное сообщение:
						  		<div class="col-12 pt-2 text-gilroy"><input class="form-control" id="decrtext" name="decrtext"></div>

				  			</div>
				  		</div>
				  		Хэш SHA-256
				  		<div class="col-12 text-gilroy"><input class="form-control" name="sha" type="text" placeholder="gdfhdghdghgd" readonly></div>
				  	</div>
			</form>
  		</div>
	</div>
	<div class="h-25"></div>
	<script>
        <!--
        function generateBtn()
        {
            generateKey()
            	.then(cryptoKeyHex => {
                // Здесь в «переменной» cryptoKeyHex находится сгенерированный ключ

                // Выводим ключ в консоль
                console.log(cryptoKeyHex);

                document.Crypt.key.value = cryptoKeyHex

                // …
              })
              .catch(e => console.log(e));
        }


        function crypt(){
        	var key = document.Crypt.key.value

                      // Поместите в переменную plainText исходный текст для шифрования
                      // при помощи стандартных средств JavaScript
	          var plainText = document.Crypt.opentext.value

	          encrypt(key, plainText)
	          .then(encryptedTextHex => {
	            // Здесь в «переменной» encryptedTextHex находится зашифрованный текст

	            // Выводим зашифрованный текст в консоль
	            console.log(encryptedTextHex);

	            document.Crypt.crypttext.value = encryptedTextHex

	            // Делаем ещё что-то полезное с зашифрованным текстом encryptedTextHex,
	            // например, выводим на экран в поле «Зашифрованный текст (выводится в формате HEX(IV+MSG))»
	            // при помощи стандартных средств JavaScript
	            // …
	          })
	          .catch(e => console.log(e));
        }


        function hash(){
        	// Поместите в переменную plainText исходный текст для шифрования
                      // при помощи стандартных средств JavaScript
                      var plainText = document.Crypt.opentext.value

                      // Вычисляем хэш от текста в переменной plainText и выполняем с ним полезные действия
                      sha256(plainText).then(digest => {
                        // Здесь в «переменной» digest находится вычисленный хэш

                        // Выводим хэш в консоль
                        console.log(digest);

                        document.Crypt.sha.placeholder = digest

                        // Делаем ещё что-то полезное с хэшом в переменной digest,
                        // например, выводим на экран в поле «Хэш SHA-256»
                        // при помощи стандартных средств JavaScript
                        // …
                      });
        }

        setInterval(hash, 100)

        function decrypted(){
        	// Поместите в переменную key ключ шифрования,
                        // полученный на этапе генерации ключа
                        var key = document.Crypt.key.value;

                        // Поместите в переменную encryptedTextHex зашифрованный текст для дешифрования
                        // при помощи стандартных средств JavaScript
                        var encryptedTextHex = document.Crypt.crypttext.value;

                        decrypt(key, encryptedTextHex)
                        .then(plainText => {
                          // Здесь в «переменной» plainText находится дешифрованный (исходный) текст

                          // Выводим дешифрованный (исходный) текст в консоль
                          console.log(plainText);


                          document.Crypt.decrtext.value = plainText 

                          // Делаем ещё что-то полезное с дешифрованным текстом plainText,
                          // например, выводим на экран в поле «Исходный текст»
                          // при помощи стандартных средств JavaScript
                          // …
                        })
                        .catch(e => console.log(e));


        }
        //-->
    </script>

	<?php include("template/footer.php");  ?>

</body>
</html>